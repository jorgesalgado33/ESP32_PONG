/*======================================================== INCLUDES =========================================================================*/
#include <Arduino.h>

#include "../lib/SBR_Global/Definition/GlobalDef.h"
/*======================================================= DEFINITION ========================================================================*/

/*======================================================== GLOBAL VARIABLES =================================================================*/

/*Test [JSA] to remove*/
#define LED_BUILTIN 2

#define WIDTH 40
#define HEIGHT 20

// Task declaration
TaskHandle_t TaskCore0, TaskCore1;
// Timer declaration
hw_timer_t * timer0 = NULL;
portMUX_TYPE timerMux0 = portMUX_INITIALIZER_UNLOCKED;

// Timer Flags
bool flagTimer0 = false;

char Table[WIDTH][HEIGHT];
uint8_t player1;
uint8_t player2;
uint8_t player1Points = 0;
uint8_t player2Points = 0;

enum Direction{
    Dir_UP,
    Dir_DOWN,
    Dir_LEFT,
    Dir_RIGHT,
};

struct Ball{
    int speed_ball;
    uint8_t Pos_J_ball;
    uint8_t Pos_I_ball;
    uint8_t Pos_J_ball_Prev;
    uint8_t Pos_I_ball_Prev;
    Direction HorizDirection;
    Direction VertDirection;
};

Ball BallPong;

void GameInit();
void DisplayTable();
void ActionButtons();
void CheckDirectionV();
void CheckDirectionH();


//======================================================== CORE LOOPS =======================================================================

// Loop of core 0
void LoopCore0( void * parameter ){
    while(true) {
        // Code for Timer 0 interruption
        if (flagTimer0){
            flagTimer0 = false;
            CheckDirectionV();
            CheckDirectionH();
            Table[BallPong.Pos_J_ball][BallPong.Pos_I_ball] = '*';
            Table[BallPong.Pos_J_ball_Prev][BallPong.Pos_I_ball_Prev] = ' ';

            BallPong.Pos_J_ball_Prev = BallPong.Pos_J_ball;
            BallPong.Pos_I_ball_Prev = BallPong.Pos_I_ball;

        }
        
        ActionButtons();
        delay(1);

    }
}

// Loop of core 1
void LoopCore1( void * parameter ){
    while(true) {
        DisplayTable();
        delay(1);
    }
}

//======================================================== TIMERS ===========================================================================

// TIMER 0
void IRAM_ATTR onTimer0(){
    portENTER_CRITICAL_ISR(&timerMux0);

    flagTimer0 = true;

    portEXIT_CRITICAL_ISR(&timerMux0);
}


//======================================================== SETUP ==============================================================================

void setup() {
    // Serial Port
    Serial.begin(921600);

    /*Test [JSA] to remove*/
    pinMode(LED_BUILTIN, OUTPUT);       // Config Output LED on board ESP32

    GameInit();
    // Task of core 0
    xTaskCreatePinnedToCore(
        LoopCore0,  /* Function to implement the task */
        "LoopCore0",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore0, /* Task handle. */
        0);         /* Core where the task should run */

    delay(500);  // needed to start-up task1

    // Task of core 1
    xTaskCreatePinnedToCore(
        LoopCore1,  /* Function to implement the task */
        "LoopCore1",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore1, /* Task handle. */
        1);         /* Core where the task should run */

    // Timer0
    Serial.println("start timer 0");
    timer0 = timerBegin(0, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer0, &onTimer0, true); // edge (not level) triggered 
    timerAlarmWrite(timer0, 100000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Enable the timer alarms
     timerAlarmEnable(timer0); // enable
}

//======================================================== MAIN LOOP ============================================================================
// Loop not used
void loop() {
    vTaskDelete(NULL);  
}

void GameInit(){
    /*table Init*/
    for(int i = 0; i< HEIGHT; i++){
        for(int j = 0; j < WIDTH; j++){
            Table[j][i]= ' ';

        }
    }

    /*player init */
    player1 = (HEIGHT/2);

    Table[0][player1-1]= '*';
    Table[0][player1]= '*';
    Table[0][player1+1]= '*';

    player2 = (HEIGHT/2);

    Table[WIDTH-1][player2-1]= '*';
    Table[WIDTH-1][player2]= '*';
    Table[WIDTH-1][player2+1]= '*';

    /*Ball init*/
    BallPong.Pos_J_ball = WIDTH/2;
    BallPong.Pos_I_ball = HEIGHT/2;
    Table[BallPong.Pos_J_ball][BallPong.Pos_I_ball]= '*';

    BallPong.Pos_I_ball_Prev = BallPong.Pos_I_ball;
    BallPong.Pos_J_ball_Prev = BallPong.Pos_J_ball;

    BallPong.VertDirection = Dir_UP;
    BallPong.HorizDirection = Dir_LEFT;
    BallPong.speed_ball = 1;


}
void DisplayTable(){

    /*Table Init*/
    for(int i=0; i< WIDTH; i++){
        if(i==uint8_t(WIDTH/4)){
            Serial.printf("%d",player1Points);
        }
        else if(i== uint8_t(3*WIDTH/4)){
            Serial.printf("%d",player2Points);
        }
        else{
            Serial.write('=');
        }
        

    }
    Serial.write("\n\r");

    for (int i = 0; i < HEIGHT; i++)
    {
        for (int j = 0; j < WIDTH; j++)
        {
            /* code */
            Serial.write(Table[j][i]);
        }
        Serial.write("\n\r");
        
        /* code */
    }
    

    /*Table End*/
    for(int i=0; i< WIDTH; i++){
        Serial.write('=');

    }

    /*Set 0,0 position*/
    Serial.printf("\033[0;0H");
}
void ActionButtons(){
     if(Serial.available()){
        char keyword = Serial.read();
        switch(keyword){
            case 'A':
            case 'a':
            if(player1 > 1){
                Table[0][player1+1] = ' ';
                Table[0][player1-2] = '*';
                player1 -=1;
            }
            break;
            case 'W':
            case 'w':
            if(player1 < HEIGHT-2){
                Table[0][player1-1] = ' ';
                Table[0][player1+2] = '*';
                player1 +=1;
            }
            break;
            case '9':
            if(player2 > 1){
                Table[WIDTH-1][player2+1] = ' ';
                Table[WIDTH-1][player2-2] = '*';
                player2 -=1;
            }
            break;
            case '3':
            if(player2 < HEIGHT-2){
                Table[WIDTH-1][player2-1] = ' ';
                Table[WIDTH-1][player2+2] = '*';
                player2 +=1;
            }
            
            break;
            

        }
    }
    

}
void CheckDirectionV(){
    if(BallPong.VertDirection ==Dir_UP){

        if(BallPong.Pos_I_ball > 0){
            BallPong.Pos_I_ball -=1;
        }
        else{
            BallPong.VertDirection = Dir_DOWN;
        }

    }
    else{//DOWN
        if(BallPong.Pos_I_ball < HEIGHT)
        {
            BallPong.Pos_I_ball +=1;
        }
        else{
            BallPong.VertDirection = Dir_UP;
        }
    }

}
void CheckDirectionH(){

    if(BallPong.HorizDirection == Dir_LEFT){

        if(BallPong.Pos_J_ball > 1){
            BallPong.Pos_J_ball -=1;
        }
        else{
            if(BallPong.Pos_J_ball == 1){
                if((BallPong.Pos_I_ball <= player1+1)&&(BallPong.Pos_I_ball >= player1-1)){

                }
                else{
                    player2Points +=1;
                    GameInit();
                }

            }


            BallPong.HorizDirection = Dir_RIGHT;
        }
        
    }
    else{//Right
        if(BallPong.Pos_J_ball <(WIDTH-2)){
            BallPong.Pos_J_ball +=1;
        }
        else{

            if(BallPong.Pos_J_ball == WIDTH-2){
                if((BallPong.Pos_I_ball <= player2+1)&&(BallPong.Pos_I_ball >= player2-1)){

                }
                else{
                    player1Points +=1;
                    GameInit();
                }

            }
            BallPong.HorizDirection = Dir_LEFT;
        }
        
    }

}